/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.Doctor.getValue())){
            organization = new DoctorOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Patient.getValue())){
            organization = new PatientOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Analyst.getValue())){
            organization = new AnalystOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Researcher.getValue())){
            organization = new ResearcherOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Drug.getValue())){
            organization = new DrugOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Chemical.getValue())){
            organization = new ChemicalOrganization();
            organizationList.add(organization);
        }
        return organization;
    }
}