/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.AppointmentDirectory;
import Business.CompoundDirectory;
import Business.DrugDirectory;
import Business.EcoSystem;
import Business.Employee.EmployeeDirectory;
import Business.Role.Role;
import Business.StudyDirectory;
import Business.UserAccount.UserAccountDirectory;
import Business.WorkQueue.WorkQueue;
import java.util.ArrayList;

/**
 *
 * @author raunak
 */
public abstract class Organization {

    private String name;
    private WorkQueue workQueue;
    private EmployeeDirectory employeeDirectory;
    private UserAccountDirectory userAccountDirectory;
    private int organizationID;
    private static int counter;
    private EcoSystem system;
    private AppointmentDirectory appointmentDirectory;
    private DrugDirectory drugDirectory;
    private CompoundDirectory compoundDirectory;
    private StudyDirectory studyDirectory;
    
    public enum Type{
        Admin("Admin Organization"), Doctor("Doctor Organization"), Drug("Drug Organization") ,Chemical ("Chemical Organization"), Patient("Patient Organization"), Researcher ("Reasercher Organization"), Analyst ("Analyst Organization");
        private String value;
        private Type(String value) {
            this.value = value;
        }
        public String getValue() {
            return value;
        }
    }

    public Organization(String name) {
        this.name = name;
        workQueue = new WorkQueue();
        employeeDirectory = new EmployeeDirectory();
        userAccountDirectory = new UserAccountDirectory();
        appointmentDirectory= new AppointmentDirectory();
        drugDirectory= new DrugDirectory();
        compoundDirectory = new CompoundDirectory();
        studyDirectory= new StudyDirectory();
        organizationID = counter;
        ++counter;
    }

    public abstract ArrayList<Role> getSupportedRole();
    
    public UserAccountDirectory getUserAccountDirectory() {
        return userAccountDirectory;
    }

    public int getOrganizationID() {
        return organizationID;
    }

    public EmployeeDirectory getEmployeeDirectory() {
        return employeeDirectory;
    }
    
    public String getName() {
        return name;
    }

    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWorkQueue(WorkQueue workQueue) {
        this.workQueue = workQueue;
    }

    public AppointmentDirectory getAppointmentDirectory() {
        return appointmentDirectory;
    }

    public DrugDirectory getDrugDirectory() {
        return drugDirectory;
    }

    public CompoundDirectory getCompoundDirectory() {
        return compoundDirectory;
    }

    public StudyDirectory getStudyDirectory() {
        return studyDirectory;
    }
    
    

    @Override
    public String toString() {
        return name;
    }
    
    
}
