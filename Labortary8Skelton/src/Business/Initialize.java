/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author amitm
 */
public class Initialize {
    
  //private ArrayList <Drug> drugList;
  //private ArrayList <Appointment> appointmentList;
  //private ArrayList <Compound> compoundList;
  private AppointmentDirectory ad;
  private DrugDirectory dd;
  private CompoundDirectory cd;
  
  
  
  public void populateDrug() throws IOException
  {
    //  drugList= new ArrayList <> ();
      String str="C:\\Users\\amitm\\Desktop\\Lab8Package\\Lab8Package\\MyFiles\\DrugData.csv";
      Scanner scan = new Scanner (new File(str));
      while (scan.hasNextLine()){
         String line = scan.nextLine();
         String[] lineArray = line.split(",");
         Drug d= dd.addDrug();
         d.setDrugName(lineArray[0]);
         d.setDrugFormula(lineArray[1]);
         d.setDrugUses(lineArray[2]);
         d.setDrugManufacturer(lineArray[3]);
         d.setDrugDescription(lineArray[4]);
         d.setDisease(lineArray[5]);
         d.setDrugState(lineArray[6]);
         //drugList.add(d); 
         }
  }
  
  public void populateAppointment() throws IOException
  {
      Calendar now=Calendar.getInstance();
      Date today= now.getTime();
      ad= new AppointmentDirectory();      
      String str="C:\\Users\\amitm\\Desktop\\Lab8Package\\Lab8Package\\MyFiles\\AppointmentData.csv";
      Scanner scan = new Scanner (new File(str));
      while (scan.hasNextLine()){
         String line = scan.nextLine();
         String[] lineArray = line.split(",");
         Appointment a = ad.addAppointment();
         a.setFname(lineArray[0]);
         a.setLname(lineArray[1]);
         a.setAge(Integer.parseInt(lineArray[2]));
         a.setGender(lineArray[3]);
         a.setCity(lineArray[4]);
         a.setHospital(lineArray[5]);
         a.setAppointmentDate(today);
         a.setDoctor(lineArray[6]);
         a.setProblem(lineArray[7]);
         a.setSeason(lineArray[8]);         
         }
  }
  
  public void populateCompound() throws IOException
  {
      //compoundList= new ArrayList<>();
      String str="C:\\Users\\amitm\\Desktop\\Lab8Package\\Lab8Package\\MyFiles\\CompoundData.csv";
      Scanner scan = new Scanner (new File(str));
      while (scan.hasNextLine()){
        String line = scan.nextLine();
        String[] lineArray = line.split(",");
        Compound c= cd.addCompound(); 
        c.setCompoundName(lineArray[0]);
        c.setMolecularFormula(lineArray[1]);
        c.setDescription(lineArray[2]);
        c.setType(lineArray[3]);
        //compoundList.add(c);
        }
      
  }
}
