/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author amitm
 */
public class Drug {
    
    private int drugNumber;
    private String drugName;
    private String drugFormula;
    private String drugUses;
    private String drugManufacturer;
    private String drugDescription;
    private String disease;
    private String drugState;
    //private int drugCatalog;
    //private String drugRoute;
    //private String drugIndication;
    private int count=0;
    public Drug()
    {
        count++;
        drugNumber=count;
    }

    public int getDrugNumber() {
        return drugNumber;
    }

    public void setDrugNumber(int drugNumber) {
        this.drugNumber = drugNumber;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getDrugFormula() {
        return drugFormula;
    }

    public void setDrugFormula(String drugFormula) {
        this.drugFormula = drugFormula;
    }

    public String getDrugUses() {
        return drugUses;
    }

    public void setDrugUses(String drugUses) {
        this.drugUses = drugUses;
    }

    public String getDrugManufacturer() {
        return drugManufacturer;
    }

    public void setDrugManufacturer(String drugManufacturer) {
        this.drugManufacturer = drugManufacturer;
    }

    public String getDrugDescription() {
        return drugDescription;
    }

    public void setDrugDescription(String drugDescription) {
        this.drugDescription = drugDescription;
    }

    public String getDisease() {
        return disease;
    }

    public void setDisease(String disease) {
        this.disease = disease;
    }

    public String getDrugState() {
        return drugState;
    }

    public void setDrugState(String drugState) {
        this.drugState = drugState;
    }

    
    
}
