/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author amitm
 */
public class DrugDirectory {
    
    private ArrayList <Drug> drugList;
    
    public DrugDirectory()
    {
        this.drugList=new ArrayList<Drug>();
    }

    public ArrayList<Drug> getDrugList() {
        return drugList;
    }

    public void setDrugList(ArrayList<Drug> drugList) {
        this.drugList = drugList;
    }
    
    public Drug addDrug()
    {
        Drug d= new Drug();
        drugList.add(d);
        return d;       
    }
    
    
}
