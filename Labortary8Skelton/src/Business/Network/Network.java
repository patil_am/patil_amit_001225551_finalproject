/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import Business.AppointmentDirectory;
import Business.Enterprise.EnterpriseDirectory;

/**
 *
 * @author amitm
 */
public class Network {
    
    private String networkName;
    private EnterpriseDirectory ed;
    
    public Network()
    {
        ed= new EnterpriseDirectory();
    }
    public String getNetworkName() {
        return networkName;
    }

    public void setNetworkName(String networkName) {
        this.networkName = networkName;
    }

    public EnterpriseDirectory getEd() {
        return ed;
    }
 

    @Override
    public String toString()
    {
        return networkName;
    }
    
}
