/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import Business.Organization.Organization;
import Business.Organization.OrganizationDirectory;

/**
 *
 * @author amitm
 */
public abstract class Enterprise extends Organization {
    
    
    private OrganizationDirectory organizationDirectory;
    
    public Enterprise(String name, EnterpriseType type)
    {
        super(name);
        this.enterpriseType=type;
        organizationDirectory= new OrganizationDirectory();
    }

    public OrganizationDirectory getOrganizationDirectory() {
        return organizationDirectory;
    }

    public void setOrganizationDirectory(OrganizationDirectory organizationDirectory) {
        this.organizationDirectory = organizationDirectory;
    }
    
    
    private EnterpriseType enterpriseType;

    public EnterpriseType getEnterpriseType() {
        return enterpriseType;
    }

    public void setEnterpriseType(EnterpriseType enterpriseType) {
        this.enterpriseType = enterpriseType;
    }
    
    
    
    public enum EnterpriseType {
        
        Hospital("Hospital"),ResearchLab ("Research Lab"),DrugManufacturer ("Drug Manufacturer"), CompoundManufacturer("Compound Manufacturer");
        
        private String value;

        private EnterpriseType(String value)
        {
            this.value=value;
        }
        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }
        
        @Override
        
        public String toString()
        {
            return value;
        }
    }
}
