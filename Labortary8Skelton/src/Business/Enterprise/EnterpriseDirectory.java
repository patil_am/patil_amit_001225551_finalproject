/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import java.util.ArrayList;

/**
 *
 * @author amitm
 */
public class EnterpriseDirectory {
    
    private ArrayList <Enterprise> enterpriseList;

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(ArrayList<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
    }
    
    public EnterpriseDirectory()
    {
        enterpriseList= new ArrayList <Enterprise>();
    }
    
    public Enterprise CreateAndAddEnterprise(String name,Enterprise.EnterpriseType type)
    {
        Enterprise enterprise=null;
        if(type==Enterprise.EnterpriseType.Hospital)
        {
            enterprise = new HospitalEnterprise(name);
            enterpriseList.add(enterprise);
        }
        if(type==Enterprise.EnterpriseType.ResearchLab)
        {
            enterprise = new ResearchLabEnterprise(name);
            enterpriseList.add(enterprise);
        }
        if(type==Enterprise.EnterpriseType.DrugManufacturer)
        {
            enterprise = new DrugManufacturerEnterprise(name);
            enterpriseList.add(enterprise);
        }
        if(type==Enterprise.EnterpriseType.CompoundManufacturer)
        {
            enterprise = new CompoundManufacturerEnterprise(name);
            enterpriseList.add(enterprise);
        }
        return enterprise;
    }
    
}
