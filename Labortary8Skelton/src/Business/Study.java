/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

/**
 *
 * @author amitm
 */
public class Study {
    
    private String studyName;
    private String studyType;
    //private String studyDesign;
    //private String studyPhase;
    //private String officialTitle;
    private String sponsoredBy;
    private String informationProvidedBy;
    private int month;

    public String getStudyName() {
        return studyName;
    }

    public void setStudyName(String studyName) {
        this.studyName = studyName;
    }

    public String getStudyType() {
        return studyType;
    }

    public void setStudyType(String studyType) {
        this.studyType = studyType;
    }

    public String getSponsoredBy() {
        return sponsoredBy;
    }

    public void setSponsoredBy(String sponsoredBy) {
        this.sponsoredBy = sponsoredBy;
    }

    public String getInformationProvidedBy() {
        return informationProvidedBy;
    }

    public void setInformationProvidedBy(String informationProvidedBy) {
        this.informationProvidedBy = informationProvidedBy;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }
    
    
}
