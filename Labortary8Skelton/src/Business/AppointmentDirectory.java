/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author amitm
 */
public class AppointmentDirectory {
    
    private  ArrayList<Appointment> appointmentList;
    //private static AppointmentDirectory ad;
    public AppointmentDirectory()
    {
        appointmentList= new ArrayList<>();
    }
    
    /*public static AppointmentDirectory getInstance()
    {
        if(ad==null)
        {
            ad= new AppointmentDirectory();
        }
        return ad;     
    }
    
    public void setAd(AppointmentDirectory ad)
    {
        this.ad=ad;
    }
 */
    public ArrayList<Appointment> getPatientList() {
        return appointmentList;
    }

    public void setPatientList(ArrayList<Appointment> patientList) {
        this.appointmentList = patientList;
    }
    
    public Appointment addAppointment()
    {
        Appointment ap= new Appointment();
        appointmentList.add(ap);
        return ap;
    }
    
    public void removeAppointment(Appointment a)
    {
       appointmentList.remove(a);
    }
    
}
