/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import Business.UserAccount.UserAccount;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author amitm
 */
public class EcoSystem extends Organization{

    private ArrayList <Network> networkList;
    private static EcoSystem business;
    public static EcoSystem getInstance()
    {
        if(business==null)
        {
            business=new EcoSystem();
        }
        return business;
    }

    private EcoSystem() {
        super(null);
        networkList= new ArrayList <Network>();
    }

    public void setEcoSystem(EcoSystem system) {
        this.business = system;
    }
    
    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public void setNetworkList(ArrayList<Network> networkList) {
        this.networkList = networkList;
    }
    
    public Network createAndAddNetwork()
    {
        Network network= new Network();
        networkList.add(network);
        return network;
    }

    
    
    @Override
    public ArrayList<Role> getSupportedRole() {
    ArrayList <Role> roleList=new ArrayList <Role> ();
    roleList.add(new SystemAdminRole());
    return roleList;
    }
    
    public boolean checkIfUserNameIsUnique(String userName)
    {
        /*if(!this.getUserAccountDirectory().checkIfUsernameIsUnique(userName)) {
            return false;
        }*/
       boolean enterpriseUserName;
       boolean organizationUserName;
       for (Network n:getNetworkList())
       {
         for (Enterprise e:n.getEd().getEnterpriseList())
         {
           enterpriseUserName=e.getUserAccountDirectory().checkIfUsernameIsUnique(userName);        
           for(Organization o:e.getOrganizationDirectory().getOrganizationList())
            {
              organizationUserName=o.getUserAccountDirectory().checkIfUsernameIsUnique(userName);
              if(enterpriseUserName==false || organizationUserName==false)
                {
                    return false;
                }                
            }
         }
       }
        return true;
    }
    
    
    
    
}
