/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.AnalystRole;

import Business.Appointment;
import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.AnalystOrganization;
import Business.Organization.ChemicalOrganization;
import Business.Organization.DrugOrganization;
import Business.Organization.Organization;
import Business.Organization.ResearcherOrganization;
import Business.Study;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PiePlot;
import org.jfree.chart.plot.PiePlot3D;
import org.jfree.data.general.DefaultPieDataset;
import org.jfree.data.general.PieDataset;

/**
 *
 * @author amitm
 */
public class ViewDashBoardJPanel extends javax.swing.JPanel {

    /**
     * Creates new form ViewDashBoardJPanel
     */
    private JPanel userProcessContainer;
    private UserAccount userAccount;
    private Enterprise enterprise;
    private EcoSystem system;
    
    public ViewDashBoardJPanel(JPanel userProcessContainer, UserAccount userAccount, Enterprise enterprise, EcoSystem system) {
        initComponents();
        this.userProcessContainer=userProcessContainer;
        this.userAccount=userAccount;
        this.enterprise=enterprise;
        this.system=system;
        populateCity();
    }
    private void populateCity()
    {
        cityJComboBox.removeAllItems();
        for(Network n:EcoSystem.getInstance().getNetworkList())
        {
            cityJComboBox.addItem(n);
        }
    }
    private void populateEnterpriseComboBox(Network network){
        enterpriseJComboBox.removeAllItems();
        
        for (Enterprise enterprise : network.getEd().getEnterpriseList()){
            enterpriseJComboBox.addItem(enterprise);
        }
        
    }
    
    int m=0;
    int f=0;
    int fall=0;
    int spring=0;
    int summer=0;
    int child=0;
    int adult=0;
    int middle=0;
    int old=0;
    
    public int getGenderMTotal()
    {
        String str= cityJComboBox.getSelectedItem().toString();
        for(Appointment a:system.getAppointmentDirectory().getPatientList())
        {
           if(a.getCity().equals(str))
           {
               if(a.getGender().equals("M"))
               {
                   m++;
               }
            }
        }
        return m;
    }
    public int getGenderFTotal()
    {
        String str= cityJComboBox.getSelectedItem().toString();
        for(Appointment a:system.getAppointmentDirectory().getPatientList())
        {
           if(a.getCity().equals(str))
           {
               if(a.getGender().equals("F"))
               {
                   f++;
               }
            }
        }
        return f;
    }
    
    public int getSeasonFTotal()
    {
        String str= cityJComboBox.getSelectedItem().toString();
        for(Appointment a:system.getAppointmentDirectory().getPatientList())
        {
           if(a.getCity().equals(str))
           {
               if(a.getSeason().equals("Fall"))
               {
                   fall++;
               }
            }
        }
        return fall;
    }
    
    public int getSeasonSumTotal()
    {
        String str= cityJComboBox.getSelectedItem().toString();
        for(Appointment a:system.getAppointmentDirectory().getPatientList())
        {
           if(a.getCity().equals(str))
           {
               if(a.getSeason().equals("Summer"))
               {
                   summer++;
               }
            }
        }
        return summer;
    }
    
    public int getSeasonSpTotal()
    {
        String str= cityJComboBox.getSelectedItem().toString();
        for(Appointment a:system.getAppointmentDirectory().getPatientList())
        {
           if(a.getCity().equals(str))
           {
               if(a.getSeason().equals("Spring"))
               {
                   spring++;
               }
            }
        }
        return spring;
    }
    
    public int getchildTotal()
    {
        String str= cityJComboBox.getSelectedItem().toString();
        for(Appointment a:system.getAppointmentDirectory().getPatientList())
        {
           if(a.getCity().equals(str))
           {
               if(a.getAge()>0 && a.getAge()<18)
               {
                   child++;
               }
            }
        }
        return child;
    }
    
    public int getAdultTotal()
    {
        String str= cityJComboBox.getSelectedItem().toString();
        for(Appointment a:system.getAppointmentDirectory().getPatientList())
        {
           if(a.getCity().equals(str))
           {
               if(a.getAge()>=18 && a.getAge()<30)
               {
                   adult++;
               }
            }
        }
        return adult;
    }
    
    public int getMiddleTotal()
    {
        String str= cityJComboBox.getSelectedItem().toString();
        for(Appointment a:system.getAppointmentDirectory().getPatientList())
        {
           if(a.getCity().equals(str))
           {
               if(a.getAge()>=31 && a.getAge()<55)
               {
                   middle++;
               }
            }
        }
        return middle;
    }
    
    public int getOldTotal()
    {
        String str= cityJComboBox.getSelectedItem().toString();
        for(Appointment a:system.getAppointmentDirectory().getPatientList())
        {
           if(a.getCity().equals(str))
           {
               if(a.getAge()>=55 && a.getAge()<90)
               {
                   old++;
               }
            }
        }
        return old;
    }
    
    public int[] getAnalystResult()
    {
        int com=0;
        int pen=0;
        int inp=0;
        int a[]= new int[3];
        Enterprise ent=(Enterprise)enterpriseJComboBox.getSelectedItem();
        Organization org=null;
        for (Organization organization:ent.getOrganizationDirectory().getOrganizationList())
        {
            if(organization instanceof AnalystOrganization)
            {
                org=organization;
                break;
            }
        }   
        if(org==null)
        {
            JOptionPane.showMessageDialog(null, "Organization doesn't exist");
        }
        else
        {
            for(WorkRequest request:org.getWorkQueue().getWorkRequestList())
            {
                if(request.getStatus().equals("Completed"))
                {
                    com++;
                }
                else if(request.getStatus().equals("Pending"))
                {
                    pen++;
                }
                else if(request.getStatus().equals("In Progress"))
                {
                    inp++;
                }
                       
                
            }
            a[0]=com;
            a[1]=pen;
            a[2]=inp;
        }
        return a;
    }
    
    public int[] getResearcherResult()
    {
        int com=0;
        int pen=0;
        int inp=0;
        int a[]= new int[3];
        Enterprise ent=(Enterprise)enterpriseJComboBox.getSelectedItem();
        Organization org=null;
        for (Organization organization:ent.getOrganizationDirectory().getOrganizationList())
        {
            if(organization instanceof ResearcherOrganization)
            {
                org=organization;
                break;
            }
        }   
        if(org==null)
        {
            JOptionPane.showMessageDialog(null, "Organization doesn't exist");
        }
        else
        {
            for(WorkRequest request:org.getWorkQueue().getWorkRequestList())
            {
                if(request.getStatus().equals("Completed"))
                {
                    com++;
                }
                else if(request.getStatus().equals("Pending"))
                {
                    pen++;
                }
                else if(request.getStatus().equals("In Progress"))
                {
                    inp++;
                }
                       
                
            }
            a[0]=com;
            a[1]=pen;
            a[2]=inp;
        }
        return a;
    }
    
    public int[] getDrugAssistantResult()
    {
        int com=0;
        int pen=0;
        int inp=0;
        int a[]= new int[3];
        Enterprise ent=(Enterprise)enterpriseJComboBox.getSelectedItem();
        Organization org=null;
        for (Organization organization:ent.getOrganizationDirectory().getOrganizationList())
        {
            if(organization instanceof DrugOrganization)
            {
                org=organization;
                break;
            }
        }   
        if(org==null)
        {
            JOptionPane.showMessageDialog(null, "Organization doesn't exist");
        }
        else
        {
            for(WorkRequest request:org.getWorkQueue().getWorkRequestList())
            {
                if(request.getStatus().equals("Completed"))
                {
                    com++;
                }
                else if(request.getStatus().equals("Pending"))
                {
                    pen++;
                }
                else if(request.getStatus().equals("In Progress"))
                {
                    inp++;
                }
                       
                
            }
            a[0]=com;
            a[1]=pen;
            a[2]=inp;
        }
        return a;
    }
    
    public int[] getChemicalAssistantResult()
    {
        int com=0;
        int pen=0;
        int inp=0;
        int a[]= new int[3];
        Enterprise ent=(Enterprise)enterpriseJComboBox.getSelectedItem();
        Organization org=null;
        for (Organization organization:ent.getOrganizationDirectory().getOrganizationList())
        {
            if(organization instanceof ChemicalOrganization)
            {
                org=organization;
                break;
            }
        }   
        if(org==null)
        {
            JOptionPane.showMessageDialog(null, "Organization doesn't exist");
        }
        else
        {
            for(WorkRequest request:org.getWorkQueue().getWorkRequestList())
            {
                if(request.getStatus().equals("Completed"))
                {
                    com++;
                }
                else if(request.getStatus().equals("Pending"))
                {
                    pen++;
                }
                else if(request.getStatus().equals("In Progress"))
                {
                    inp++;
                }
                       
                
            }
            a[0]=com;
            a[1]=pen;
            a[2]=inp;
        }
        return a;
    }
    
    public int[] getStudyType()
    {
        int a1=0;
        int a2=0;
        int a3=0;
        int a4=0;
        int a5=0;
        int a[]= new int[5];
        
        for(Study s:system.getStudyDirectory().getStudyList())
        {
            if(s.getStudyType().equals("Drug Study"))
            {
                a1++;
            }
            else if(s.getStudyType().equals("Compound Study"))
            {
                a2++;
            }
            else if(s.getStudyType().equals("Gene Study"))
            {
                a3++;
            }
            else if(s.getStudyType().equals("Disease Study"))
            {
                a4++;
            }
            else
            {
                a5++;
            }
        }
        a[0]=a1;
        a[1]=a2;
        a[2]=a3;
        a[3]=a4;
        a[4]=a5;
        
        return a;
        
    }
    
    public int[] getSponsorType()
    {
        int a1=0;
        int a2=0;
        int a3=0;
        int a4=0;
        int a5=0;
        int a[]= new int[5];
        
        for(Study s:system.getStudyDirectory().getStudyList())
        {
            if(s.getSponsoredBy().equals("Government"))
            {
                a1++;
            }
            else if(s.getSponsoredBy().equals("Individual"))
            {
                a2++;
            }
            else if(s.getSponsoredBy().equals("Corporate"))
            {
                a3++;
            }
            else if(s.getSponsoredBy().equals("Private"))
            {
                a4++;
            }
            else
            {
                a5++;
            }
        }
        a[0]=a1;
        a[1]=a2;
        a[2]=a3;
        a[3]=a4;
        a[4]=a5;
        
        return a;
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        networkJBtn = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        seasonJBtn = new javax.swing.JButton();
        agegroupJBtn = new javax.swing.JButton();
        cityJComboBox = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        studyJBtn = new javax.swing.JButton();
        sponsorJBtn = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        analystJBtn = new javax.swing.JButton();
        researcherJBtn = new javax.swing.JButton();
        drugManufacturerJBtn = new javax.swing.JButton();
        chemicalManufacturerJBtn = new javax.swing.JButton();
        enterpriseJComboBox = new javax.swing.JComboBox();
        jLabel9 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        backJBtn = new javax.swing.JButton();

        networkJBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        networkJBtn.setText("Gender");
        networkJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                networkJBtnActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 3, 18)); // NOI18N
        jLabel1.setText("Report DashBoard");

        seasonJBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        seasonJBtn.setText("Season");
        seasonJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                seasonJBtnActionPerformed(evt);
            }
        });

        agegroupJBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        agegroupJBtn.setText("Age Group");
        agegroupJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                agegroupJBtnActionPerformed(evt);
            }
        });

        cityJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cityJComboBoxActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        jLabel2.setText("City:");

        jLabel4.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        jLabel4.setText("Study Reports:");

        studyJBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        studyJBtn.setText("Study Type");
        studyJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                studyJBtnActionPerformed(evt);
            }
        });

        sponsorJBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        sponsorJBtn.setText("Sponsor");
        sponsorJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sponsorJBtnActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        jLabel3.setText("Work Request Reports:");

        analystJBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        analystJBtn.setText("Analyst");
        analystJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                analystJBtnActionPerformed(evt);
            }
        });

        researcherJBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        researcherJBtn.setText("Researcher");
        researcherJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                researcherJBtnActionPerformed(evt);
            }
        });

        drugManufacturerJBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        drugManufacturerJBtn.setText("Drug Manufacturer");
        drugManufacturerJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                drugManufacturerJBtnActionPerformed(evt);
            }
        });

        chemicalManufacturerJBtn.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        chemicalManufacturerJBtn.setText("Chemical Manufacturer");
        chemicalManufacturerJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chemicalManufacturerJBtnActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Cambria", 3, 24)); // NOI18N
        jLabel9.setText("HealthServe");

        jLabel8.setFont(new java.awt.Font("Cambria", 1, 13)); // NOI18N
        jLabel8.setText("Analyse.Mobilize.Serve");

        jLabel5.setFont(new java.awt.Font("Tahoma", 3, 12)); // NOI18N
        jLabel5.setText("Enterprise:");

        backJBtn.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        backJBtn.setText("<<BACK");
        backJBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backJBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(286, 286, 286)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(51, 51, 51)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel8)
                                .addComponent(jLabel9)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(analystJBtn)
                                    .addGap(47, 47, 47)
                                    .addComponent(researcherJBtn)
                                    .addGap(56, 56, 56)
                                    .addComponent(drugManufacturerJBtn)
                                    .addGap(49, 49, 49)
                                    .addComponent(chemicalManufacturerJBtn))
                                .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                    .addComponent(jLabel5)
                                    .addGap(31, 31, 31)
                                    .addComponent(enterpriseJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addGap(26, 26, 26)
                                        .addComponent(cityJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(networkJBtn)
                                        .addGap(42, 42, 42)
                                        .addComponent(seasonJBtn)
                                        .addGap(35, 35, 35)
                                        .addComponent(agegroupJBtn)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(studyJBtn)
                                        .addGap(40, 40, 40)
                                        .addComponent(sponsorJBtn))))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(backJBtn)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(cityJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(networkJBtn)
                    .addComponent(seasonJBtn)
                    .addComponent(agegroupJBtn)
                    .addComponent(studyJBtn)
                    .addComponent(sponsorJBtn))
                .addGap(80, 80, 80)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(enterpriseJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(35, 35, 35)
                .addComponent(jLabel3)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(analystJBtn)
                    .addComponent(researcherJBtn)
                    .addComponent(drugManufacturerJBtn)
                    .addComponent(chemicalManufacturerJBtn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 176, Short.MAX_VALUE)
                .addComponent(backJBtn)
                .addGap(1, 1, 1)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addGap(23, 23, 23))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void networkJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_networkJBtnActionPerformed
        // TODO add your handling code here:
        String str=cityJComboBox.getSelectedItem().toString();
        int male=getGenderMTotal();
        int female=getGenderFTotal();
        DefaultPieDataset pieDataset=new DefaultPieDataset();
        pieDataset.setValue("Male",new Integer(male));
        pieDataset.setValue("Female",new Integer(female));
        JFreeChart chart=ChartFactory.createPieChart("Male vs Female Patient", pieDataset, true, true, true);
        PiePlot P=(PiePlot)chart.getPlot();
        //P.setForegroundAlpha(TOP_ALIGNMENT);
        ChartFrame frame=new ChartFrame(str+" Patient Pie Chart",chart);
        frame.setVisible(true);
        frame.setSize(500, 500);
    }//GEN-LAST:event_networkJBtnActionPerformed

    private void seasonJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_seasonJBtnActionPerformed
        // TODO add your handling code here:
        String str=cityJComboBox.getSelectedItem().toString();
        int fall=getSeasonFTotal();
        int spring=getSeasonSpTotal();
        int summer=getSeasonSumTotal();
        DefaultPieDataset pieDataset=new DefaultPieDataset();
        pieDataset.setValue("Fall Patient",new Integer(fall));
        pieDataset.setValue("Spring Patient",new Integer(spring));
        pieDataset.setValue("Summer Patient",new Integer(summer));
        JFreeChart chart=ChartFactory.createPieChart("Fall vs Spring vs Summer", pieDataset, true, true, true);
        PiePlot P=(PiePlot)chart.getPlot();
        //P.setForegroundAlpha(TOP_ALIGNMENT);
        ChartFrame frame=new ChartFrame(str+" Season Pie Chart",chart);
        frame.setVisible(true);
        frame.setSize(500, 500);
    }//GEN-LAST:event_seasonJBtnActionPerformed

    private void agegroupJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_agegroupJBtnActionPerformed
        // TODO add your handling code here:
        String str=cityJComboBox.getSelectedItem().toString();
        int ch=getchildTotal();
        int ad=getAdultTotal();
        int mi=getMiddleTotal();
        int olda=getOldTotal();
        DefaultPieDataset pieDataset=new DefaultPieDataset();
        pieDataset.setValue("Children",new Integer(ch));
        pieDataset.setValue("Adult",new Integer(ad));
        pieDataset.setValue("Middle age",new Integer(mi));
        pieDataset.setValue("Old age",new Integer(olda));
        JFreeChart chart=ChartFactory.createPieChart("Child vs Adult vs Middle Age vs Old Age ", pieDataset, true, true, true);
        PiePlot P=(PiePlot)chart.getPlot();
        //P.setForegroundAlpha(TOP_ALIGNMENT);
        ChartFrame frame=new ChartFrame(str+" Age group Pie Chart",chart);
        frame.setVisible(true);
        frame.setSize(500, 500);
    }//GEN-LAST:event_agegroupJBtnActionPerformed

    private void analystJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_analystJBtnActionPerformed
        // TODO add your handling code here:
        int b[]=new int[3];
        b=getAnalystResult();
        int com=b[0];
        int pen=b[1];
        int ip=b[2];
        String str=cityJComboBox.getSelectedItem().toString();
        DefaultPieDataset pieDataset=new DefaultPieDataset();
        pieDataset.setValue("Completed",new Integer(com));
        pieDataset.setValue("Pending",new Integer(pen));
        pieDataset.setValue("In Progress",new Integer(ip));
        JFreeChart chart=ChartFactory.createPieChart("Analyst request status", pieDataset, true, true, true);
        PiePlot P=(PiePlot)chart.getPlot();
        //P.setForegroundAlpha(TOP_ALIGNMENT);
        ChartFrame frame=new ChartFrame(str+" Analyst report",chart);
        frame.setVisible(true);
        frame.setSize(500, 500);
    }//GEN-LAST:event_analystJBtnActionPerformed

    private void researcherJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_researcherJBtnActionPerformed
        // TODO add your handling code here:
        int b[]=new int[3];
        b=getResearcherResult();
        int com=b[0];
        int pen=b[1];
        int ip=b[2];
        String str=cityJComboBox.getSelectedItem().toString();
        DefaultPieDataset pieDataset=new DefaultPieDataset();
        pieDataset.setValue("Completed",new Integer(com));
        pieDataset.setValue("Pending",new Integer(pen));
        pieDataset.setValue("In Progress",new Integer(ip));
        JFreeChart chart=ChartFactory.createPieChart("Researcher request status ", pieDataset, true, true, true);
        PiePlot P=(PiePlot)chart.getPlot();
        //P.setForegroundAlpha(TOP_ALIGNMENT);
        ChartFrame frame=new ChartFrame(str+" Researcher Report",chart);
        frame.setVisible(true);
        frame.setSize(500, 500);
    }//GEN-LAST:event_researcherJBtnActionPerformed

    private void drugManufacturerJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_drugManufacturerJBtnActionPerformed
        // TODO add your handling code here:
        int b[]=new int[3];
        b=getDrugAssistantResult();
        int com=b[0];
        int pen=b[1];
        int ip=b[2];
        String str=cityJComboBox.getSelectedItem().toString();
        DefaultPieDataset pieDataset=new DefaultPieDataset();
        pieDataset.setValue("Completed",new Integer(com));
        pieDataset.setValue("Pending",new Integer(pen));
        pieDataset.setValue("In Progress",new Integer(ip));
        JFreeChart chart=ChartFactory.createPieChart("Drug Assistant request status ", pieDataset, true, true, true);
        PiePlot P=(PiePlot)chart.getPlot();
        //P.setForegroundAlpha(TOP_ALIGNMENT);
        ChartFrame frame=new ChartFrame(str+" Drug Assistant report",chart);
        frame.setVisible(true);
        frame.setSize(500, 500);
    }//GEN-LAST:event_drugManufacturerJBtnActionPerformed

    private void chemicalManufacturerJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chemicalManufacturerJBtnActionPerformed
        // TODO add your handling code here:
        int b[]=new int[3];
        b=getChemicalAssistantResult();
        int com=b[0];
        int pen=b[1];
        int ip=b[2];
        String str=cityJComboBox.getSelectedItem().toString();
        DefaultPieDataset pieDataset=new DefaultPieDataset();
        pieDataset.setValue("Completed",new Integer(com));
        pieDataset.setValue("Pending",new Integer(pen));
        pieDataset.setValue("In Progress",new Integer(ip));
        JFreeChart chart=ChartFactory.createPieChart("Chemical Assistant request status ", pieDataset, true, true, true);
        PiePlot P=(PiePlot)chart.getPlot();
        //P.setForegroundAlpha(TOP_ALIGNMENT);
        ChartFrame frame=new ChartFrame(str+" Chemical Assistant report",chart);
        frame.setVisible(true);
        frame.setSize(500, 500);
    }//GEN-LAST:event_chemicalManufacturerJBtnActionPerformed

    private void cityJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cityJComboBoxActionPerformed
        // TODO add your handling code here:
        Network net=(Network)cityJComboBox.getSelectedItem();
        populateEnterpriseComboBox(net);
    }//GEN-LAST:event_cityJComboBoxActionPerformed

    private void studyJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_studyJBtnActionPerformed
        // TODO add your handling code here:
        //String str=cityJComboBox.getSelectedItem().toString();
        int b[]= new int[5];
        b=getStudyType();
        int dr=b[0];
        int di=b[1];
        int dg=b[2];
        int co=b[3];
        int ot=b[4];
        DefaultPieDataset pieDataset=new DefaultPieDataset();
        pieDataset.setValue("Drug Study",new Integer(dr));
        pieDataset.setValue("Compound Study",new Integer(di));
        pieDataset.setValue("Gene Study",new Integer(dg));
        pieDataset.setValue("Disease Study",new Integer(co));
        pieDataset.setValue("Other Study",new Integer(ot));
        JFreeChart chart=ChartFactory.createPieChart("Study Type Pie Chart", pieDataset, true, true, true);
        PiePlot P=(PiePlot)chart.getPlot();
        //P.setForegroundAlpha(TOP_ALIGNMENT);
        ChartFrame frame=new ChartFrame("Study Report",chart);
        frame.setVisible(true);
        frame.setSize(500, 500);
    }//GEN-LAST:event_studyJBtnActionPerformed

    private void sponsorJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_sponsorJBtnActionPerformed
        // TODO add your handling code here:
        int b[]= new int[5];
        b=getStudyType();
        int gov=b[0];
        int ind=b[1];
        int cor=b[2];
        int pri=b[3];
        int oth=b[4];
        DefaultPieDataset pieDataset=new DefaultPieDataset();
        pieDataset.setValue("Government Sponsorship",new Integer(gov));
        pieDataset.setValue("Individual Sponsorship",new Integer(ind));
        pieDataset.setValue("Corporate Sponsorship",new Integer(cor));
        pieDataset.setValue("Private Sponsorship",new Integer(pri));
        pieDataset.setValue("Other Sponsorship",new Integer(oth));
        JFreeChart chart=ChartFactory.createPieChart("Study Sponsorship Pie Chart", pieDataset, true, true, true);
        PiePlot P=(PiePlot)chart.getPlot();
        //P.setForegroundAlpha(TOP_ALIGNMENT);
        ChartFrame frame=new ChartFrame("Study Report",chart);
        frame.setVisible(true);
        frame.setSize(500, 500);
    }//GEN-LAST:event_sponsorJBtnActionPerformed

    private void backJBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backJBtnActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);

    }//GEN-LAST:event_backJBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton agegroupJBtn;
    private javax.swing.JButton analystJBtn;
    private javax.swing.JButton backJBtn;
    private javax.swing.JButton chemicalManufacturerJBtn;
    private javax.swing.JComboBox cityJComboBox;
    private javax.swing.JButton drugManufacturerJBtn;
    private javax.swing.JComboBox enterpriseJComboBox;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JButton networkJBtn;
    private javax.swing.JButton researcherJBtn;
    private javax.swing.JButton seasonJBtn;
    private javax.swing.JButton sponsorJBtn;
    private javax.swing.JButton studyJBtn;
    // End of variables declaration//GEN-END:variables
}
